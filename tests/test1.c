/*
 * Copyright (c) 2015-2017 Devin Smith <devin@devinsmith.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>

#include <X11/StringDefs.h>
#include <X11/Intrinsic.h>
#include <X11/Core.h>
#include <X11/Object.h>
#include <X11/Shell.h>
#include <Command.h>
#include <AsciiText.h>
#include <Paned.h>

static void quit();

static XtAppContext app_ctx;
static Display *dpy;

static String fallback[] = {
  "*background: gray",
  NULL
};

struct chat {
  int connected;
  int fd;
  Widget output;
};

static const char *startup_text = "Use /connect to connect to the chat.";

static void
quit(Widget w, XtPointer client, XtPointer call)
{
  exit(0);
}

static void
write_to_output(struct chat *chat, const char *data)
{
  char *params[1];
  params[0] = (char *)data;

  XtCallActionProc(chat->output, "next-line",
              NULL, NULL, 0);
  XtCallActionProc(chat->output, "insert-string",
              NULL, params, 1);
}

static void
input_line_key(Widget input, XtPointer data, XEvent *ev, Boolean *dispatch)
{
  Arg args[1];
  char *str = NULL;
  struct chat *chat = data;

  if (XLookupKeysym(&ev->xkey, 0) == XK_Return) {
    XtSetArg(args[0], XtNstring, &str);
    XtGetValues(input, args, 1);

    if (str[0] == '/') {
      /* Process commands */
      if (strcmp(str + 1, "connect") == 0) {
        /* do connect */
      } else {
        write_to_output(chat, "Unknown command\n");
      }

    } else {
      /* do_send(); */
    }

    printf("You typed: %s\n", str);

    /* Clear input string */
    XtVaSetValues(input, XtNstring, "", NULL);
    *dispatch = FALSE;
  }
}

int
main(int argc, char *argv[])
{
  Widget toplevel;
  Widget command;
  Arg args[10];
  int nargs;
  struct chat chat_ctx = { 0 };

#if 0
  toplevel = XtAppInitialize(&app_ctx, "simple", NULL, 0, &argc,
      argv, NULL, NULL, 0);
#endif
  /* XtAppInitialize() does all these things. */
  /* Initialize X11 Intrinsics toolkit */
  XtToolkitInitialize();
  app_ctx = XtCreateApplicationContext();
  XtAppSetFallbackResources(app_ctx, fallback);
  dpy = XtOpenDisplay(app_ctx, NULL, "chat", "chat", NULL, 0, &argc, argv);
  toplevel = XtVaAppCreateShell("chat", "chat",
      applicationShellWidgetClass, dpy, NULL);

  nargs = 0;
  Widget pane = XtCreateManagedWidget("pane", panedWidgetClass, toplevel,
    args, nargs);

  /* Create output window */
  XtSetArg(args[nargs], XtNeditType, XawtextEdit);         nargs++;
  XtSetArg(args[nargs], XtNstring, startup_text);          nargs++;
  XtSetArg(args[nargs], XtNwidth, 400);                    nargs++;
  XtSetArg(args[nargs], XtNheight, 200);                   nargs++;
  XtSetArg(args[nargs], XtNwrap, XawtextWrapLine);         nargs++;
  chat_ctx.output = XtCreateManagedWidget("output", asciiTextWidgetClass,
    pane, args, nargs);
  XtVaSetValues(chat_ctx.output, XtNinsertPosition, strlen(startup_text), NULL);

  nargs = 0;
  XtSetArg(args[nargs], XtNeditType,   XawtextEdit);   nargs++;
  XtSetArg(args[nargs], XtNskipAdjust, TRUE);      nargs++;

  Widget input = XtCreateManagedWidget("input", asciiTextWidgetClass,
    pane, args, nargs);
  /* Handle key release mask */
  XtAddEventHandler(input, KeyReleaseMask, TRUE, input_line_key, &chat_ctx);


  command = XtCreateManagedWidget("Exit",
     commandWidgetClass, pane, NULL, 0);

  XtAddCallback(command, XtNcallback, quit, NULL);
  XtRealizeWidget(toplevel);
  XtSetKeyboardFocus(toplevel, input);
  XtAppMainLoop(app_ctx);

  return 0;
}


