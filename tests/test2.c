/*
 * Copyright (c) 2015-2017 Devin Smith <devin@devinsmith.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>

#include <X11/StringDefs.h>
#include <X11/Intrinsic.h>
#include <X11/Core.h>
#include <X11/Object.h>
#include <X11/Shell.h>
#include <Command.h>

static void quit();

static XtAppContext app_ctx;
static Display *dpy;

static String fallback[] = {
  "*background: gray",
  NULL
};

static void
quit(Widget w, XtPointer client, XtPointer call)
{
  exit(0);
}

int
main(int argc, char *argv[])
{
  Widget toplevel;
  Widget command;

  /* Initialize X11 Intrinsics toolkit */
  XtToolkitInitialize();
  app_ctx = XtCreateApplicationContext();
  XtAppSetFallbackResources(app_ctx, fallback);
  dpy = XtOpenDisplay(app_ctx, NULL, "chat", "chat", NULL, 0, &argc, argv);
  toplevel = XtVaAppCreateShell("chat", "chat",
      applicationShellWidgetClass, dpy, NULL);

  command = XtCreateManagedWidget("Exit",
     commandWidgetClass, toplevel, NULL, 0);

  XtAddCallback(command, XtNcallback, quit, NULL);
  XtRealizeWidget(toplevel);
  XtAppMainLoop(app_ctx);

  return 0;
}


